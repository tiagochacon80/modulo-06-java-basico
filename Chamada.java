package modulo6_vetores_colecoes;


public class Chamada {
    public static void main (String[] args){       
		
		String[][] chamada = new String[5][4];
		
		chamada[0][0] = "Ausente";
        chamada[0][1] = "Presente";
        chamada[0][2] = "Presente";
        chamada[0][3] = "Presente";     

        chamada[1][0] = "Presente";
        chamada[1][1] = "Ausente";
        chamada[1][2] = "Presente";
        chamada[1][3] = "Ausente";      

        chamada[2][0] = "Presente";
        chamada[2][1] = "Presente";
        chamada[2][2] = "Presente";
        chamada[2][3] = "Presente";    
        
        chamada[3][0] = "Presente";
        chamada[3][1] = "Presente";
        chamada[3][2] = "Presente";
        chamada[3][3] = "Presente";
        
        chamada[4][0] = "Presente";
        chamada[4][1] = "Ausente";
        chamada[4][2] = "Presente";
        chamada[4][3] = "Ausente"; 
        
        System.out.println("Lista de chamada: ");
        System.out.println();
        for(int i=0; i<chamada.length; i++) {
        	for (int j=0; j<chamada[i].length; j++) {
        		System.out.print(chamada[i][j] + " ");
        	}
        	System.out.println();
        }        
        
    }
}
